<?php
namespace API\Users;
use API\BDD\ConnexionBDD;
use PDO;
require '../autoload.php';

$idR = $_GET['idR'];
$idU = $_GET['idU'];
$date = $_GET['date'];
$heure = $_GET['heure'];
$nbPersonne = $_GET['nbPersonnes'];
$nomReservation = $_GET['nomReservation'];

$pdo = ConnexionBDD::getConnexion();

$requete = "INSERT INTO reservation (idResto, idUtilisateur, date, Heure, nbPersonnes, nomReservation) VALUES (:idR, :idU, :date, :heure, :nbPersonnes, :nomReservation)";
$stmt = $pdo->prepare($requete);

$stmt->bindParam(':idR', $idR, PDO::PARAM_INT);
$stmt->bindParam(':idU', $idU, PDO::PARAM_INT);
$stmt->bindParam(':date', $date, PDO::PARAM_STR);
$stmt->bindParam(':heure', $heure, PDO::PARAM_STR);
$stmt->bindParam(':nbPersonnes', $nbPersonne, PDO::PARAM_INT);
$stmt->bindParam(':nomReservation', $nomReservation, PDO::PARAM_STR);


$ok = $stmt->execute();

if ($ok) {
   $message = array(
       'code' => 'success',
   );
    echo json_encode($message);
}