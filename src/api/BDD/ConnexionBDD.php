<?php
    namespace API\BDD;
    use PDO;
    use PDOException;

    class ConnexionBDD {
        private static $pdo = null;
        private static $host = 'localhost';
        private static $dbname = '2slamprj2eq02';
        private static $user = 'util_2slamprj2eq02';
        private static $password = 'MhgZddJ6yUpB255';
        private function __construct() {}

        public static function getConnexion() {
            if (is_null(self::$pdo)) {
                try {
                    self::$pdo = new PDO('mysql:host=' . self::$host . ';dbname=' . self::$dbname, self::$user, self::$password);
                } catch (PDOException $e) {
                    echo 'Connexion échouée : ' . $e->getMessage();
                }
            } else {
                echo 'Connexion déjà établie';
            }
            return self::$pdo;
        }
    }
?>