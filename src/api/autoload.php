<?php
namespace API;

spl_autoload_register(function ($class) {
    $class = str_replace(__NAMESPACE__ . '\\', '', $class);
    $class = str_replace('\\', '/', $class);
    require __DIR__ . '/' . $class . '.php';
});

header('Content-Type: application/json; charset=utf-8');
