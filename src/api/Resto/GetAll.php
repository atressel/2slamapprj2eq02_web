<?php
namespace API\Users;
use API\BDD\ConnexionBDD;
use PDO;
require '../autoload.php';

$pdo = ConnexionBDD::getConnexion(); 

$requete = "SELECT * FROM resto"; 
$requetesPhotos = "SELECT * FROM photo WHERE idR = :idR"; 

$stmt = $pdo->prepare($requete); 

$ok = $stmt->execute(); 

$datas = array();
if ($ok) { 
    while ($ligne = $stmt->fetch(PDO::FETCH_ASSOC)) { 
        unset($ligne['horairesR']);
        $datas['resto'][] = $ligne;
        $idR = $ligne['idR'];
        $stmtPhotos = $pdo->prepare($requetesPhotos);
        $stmtPhotos->bindParam(':idR', $idR, PDO::PARAM_INT);
        $okPhotos = $stmtPhotos->execute();
        if ($okPhotos) {
            while ($lignePhotos = $stmtPhotos->fetch(PDO::FETCH_ASSOC)) {
                $datas['resto'][count($datas['resto']) - 1]['photos'][] = $lignePhotos;
            }
        }
    }
    echo json_encode($datas, JSON_PRETTY_PRINT);
}
