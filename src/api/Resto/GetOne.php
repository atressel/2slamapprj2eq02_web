<?php
namespace API\Users;
use API\BDD\ConnexionBDD;
use PDO;
require '../autoload.php';

$pdo = ConnexionBDD::getConnexion(); 

$idR = $_GET['idR'];

$requete = "SELECT * FROM resto WHERE idR = :idR"; 
$requetesPhotos = "SELECT * FROM photo WHERE idR = :idR"; 
$requeteTypeCuisines = "SELECT tc.libelleTC FROM typecresto INNER JOIN typescuisine tc ON tc.idTC = typecresto.idTC WHERE idR = :idR"; 

$stmt = $pdo->prepare($requete); 
$stmt->bindParam(':idR', $idR, PDO::PARAM_INT);

$ok = $stmt->execute(); 

$datas = array();
if ($ok) { 
    while ($ligne = $stmt->fetch(PDO::FETCH_ASSOC)) { 
        unset($ligne['horairesR']);
        $datas['resto'] = $ligne;
        $stmtPhotos = $pdo->prepare($requetesPhotos);
        $stmtPhotos->bindParam(':idR', $idR, PDO::PARAM_INT);
        $okPhotos = $stmtPhotos->execute();
        if ($okPhotos) {
            while ($lignePhotos = $stmtPhotos->fetch(PDO::FETCH_ASSOC)) {
                
                $datas['resto']['photos'][] = $lignePhotos;
            }
        }
        $stmtTypeCuisines = $pdo->prepare($requeteTypeCuisines);
        $stmtTypeCuisines->bindParam(':idR', $idR, PDO::PARAM_INT);
        $okTypeCuisines = $stmtTypeCuisines->execute();
        if ($okTypeCuisines) {
            while ($ligneTypeCuisines = $stmtTypeCuisines->fetch(PDO::FETCH_ASSOC)) {
                
                $datas['resto']['typeCuisines'][] = $ligneTypeCuisines;
            }
        }

    }
    echo json_encode($datas);
}
