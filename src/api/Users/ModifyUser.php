<?php
namespace API\Users;
use API\BDD\ConnexionBDD;
use PDO;
require '../autoload.php';

$pdo = ConnexionBDD::getConnexion();

$idU = $_GET['idU'];
$mailU = $_GET['mailU'];
$nomU = $_GET['nomU'];
$prenomU = $_GET['prenomU'];
$telU = $_GET['telU'];

$requete = $pdo->prepare("UPDATE utilisateur SET mailU = :mailU, nomU = :nomU, prenomU = :prenomU, telU = :telU WHERE idU = :idU");
$requete->bindParam(':idU', $idU, PDO::PARAM_INT);
$requete->bindParam(':mailU', $mailU, PDO::PARAM_STR);
$requete->bindParam(':nomU', $nomU, PDO::PARAM_STR);
$requete->bindParam(':prenomU', $prenomU, PDO::PARAM_STR);
$requete->bindParam(':telU', $telU, PDO::PARAM_INT);

$requete->execute();

if($requete->rowCount() > 0) {
    $message = array(
        'code' => 'success',
    );
    echo json_encode($message);
} else {
    $message = array(
        'code' => 'not_found',
    );
    echo json_encode($message);
}

?>