<?php
namespace API\Users;
use API\BDD\ConnexionBDD;
use PDO;
require '../autoload.php';


$mailU = $_GET['mailU'];
$mdpU = $_GET['mdpU'];

$pdo = ConnexionBDD::getConnexion(); 

$requete = "SELECT * FROM utilisateur WHERE mailU = :mailU"; 

$stmt = $pdo->prepare($requete); 

$stmt->bindParam(':mailU', $mailU, PDO::PARAM_STR);

$ok = $stmt->execute(); 

if ($ok) { 
    $ligne = $stmt->fetch(PDO::FETCH_ASSOC);
    if ($ligne) {
        if (password_verify($mdpU, $ligne['mdpU'])) {
            $ligne['code'] = 'success';
            echo json_encode($ligne);
        } else {
            $message = array(
                'code' => 'not_found',
            );
            echo json_encode($message);
        }
    } else {
        $message = array(
            'code' => 'not_found',
        );
        echo json_encode($message);
    }
} else {
    echo "ko";
}