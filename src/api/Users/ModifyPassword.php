<?php
namespace API\Users;
use API\BDD\ConnexionBDD;
use PDO;
require '../autoload.php';

$pdo = ConnexionBDD::getConnexion();

$idU = $_GET['idU'];
$oldPassword = $_GET['oldPassword'];
$newPassword = $_GET['newPassword'];

$requete = "SELECT * FROM utilisateur WHERE idU = :idU";
$stmt = $pdo->prepare($requete);
$stmt->bindParam(':idU', $idU, PDO::PARAM_INT);
$ok = $stmt->execute();

if ($ok) {
    $ligne = $stmt->fetch(PDO::FETCH_ASSOC);
}

if(password_verify($oldPassword, $ligne['mdpU'])) {
    $password = password_hash($newPassword, PASSWORD_DEFAULT);

    $requete = $pdo->prepare("UPDATE utilisateur SET mdpU = :mdpU WHERE idU = :idU");
    $requete->bindParam(':idU', $idU, PDO::PARAM_INT);
    $requete->bindParam(':mdpU', $password, PDO::PARAM_STR);
    $requete->execute();

    if($requete->rowCount() > 0) {
        $message = array(
            'code' => 'success',
        );
        echo json_encode($message);
    } else {
        $message = array(
            'code' => 'error',
        );
        echo json_encode($message);
    }
} else {
    $message = array(
        'code' => 'bad_password',
    );
    echo json_encode($message);
}

?>