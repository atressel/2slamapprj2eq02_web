<?php
namespace API\Users;
use API\BDD\ConnexionBDD;
use PDO;
require '../autoload.php';

$mailU = $_GET['mailU'];
$mdpU = $_GET['mdpU'];

$pdo = ConnexionBDD::getConnexion();

$requete = "SELECT * FROM utilisateur WHERE mailU = :mailU";
$stmt = $pdo->prepare($requete);

$stmt->bindParam(':mailU', $mailU, PDO::PARAM_STR);

$ok = $stmt->execute();

if ($ok) {
    $ligne = $stmt->fetch(PDO::FETCH_ASSOC);
    if ($ligne) {

        if ($ligne['mailU'] == $mailU) {
            $message = array(
                'code' => 'mail_already_used',
            );
            echo json_encode($message);
        } else {
            $message = array(
                'code' => 'pseudo_already_used',
            );
            echo json_encode($message);
        }
    } else {
        $requete = "INSERT INTO utilisateur (mailU, mdpU) VALUES (:mailU, :mdpU)";
        $stmt = $pdo->prepare($requete); 

        $mdpU = password_hash($mdpU, PASSWORD_DEFAULT);

        $stmt->bindParam(':mailU', $mailU, PDO::PARAM_STR);
        $stmt->bindParam(':mdpU', $mdpU, PDO::PARAM_STR);

        $ok = $stmt->execute();

        if ($ok) {
            $requete = "SELECT * FROM utilisateur WHERE mailU = :mailU";
            $stmt = $pdo->prepare($requete); 

            $stmt->bindParam(':mailU', $mailU, PDO::PARAM_STR);

            $ok = $stmt->execute();

            if ($ok) {
                $ligne = $stmt->fetch(PDO::FETCH_ASSOC);
                if ($ligne) {
                    $ligne['code'] = 'success';
                    echo json_encode($ligne);
                } else {
                    echo "ko";
                }
            } else {
                echo "ko";
            }
        } else {
            echo "ko";
        }
    }
} else {
    echo "ko";
}

