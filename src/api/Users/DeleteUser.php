<?php
namespace API\Users;
use API\BDD\ConnexionBDD;
use PDO;
require '../autoload.php';

$pdo = ConnexionBDD::getConnexion();

$idU = $_GET['idU'];

$requete = $pdo->prepare("DELETE FROM utilisateur WHERE idU = :idU");
$requete->bindParam(':idU', $idU, PDO::PARAM_INT);

$requete->execute();

if($requete->rowCount() > 0) {
    $message = array(
        'code' => 'success',
    );
    echo json_encode($message);
} else {
    $message = array(
        'code' => 'not_found',
    );
    echo json_encode($message);
}

?>