<?php
namespace Tests;

// En tant qu'utilisateur, je veux pouvoir récupérer tous les restaurants de la base de données.
echo '<h3>GetAll</h3>';
echo "En tant qu'utilisateur, je veux pouvoir récupérer tous les restaurants de la base de données.";

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "http://localhost/2slamprj2eq02/src/api/resto/getAll.php");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$response = curl_exec($ch);
curl_close($ch);

$response = json_decode($response, true);

echo '<pre>';
echo 'Response: ';
print_r($response);
echo '</pre>';

// En tant qu'utilisateur, je veux pouvoir récupérer le restaurant qui a l'id 1 de la base de données.
echo '<h3>GetOne</h3>';
echo "En tant qu'utilisateur, je veux pouvoir récupérer le restaurant qui a l'id 1 de la base de données.";

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "http://localhost/2slamprj2eq02/src/api/resto/getOne.php?idR=1");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$response = curl_exec($ch);
curl_close($ch);

$response = json_decode($response, true);

echo '<pre>';
echo 'Response: ';
print_r($response);
echo '</pre>';