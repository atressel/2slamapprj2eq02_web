<?php
namespace Tests;

// En tant qu’utilisateur, je souhaite pouvoir me créer un compte avec mon adresse mail et un mot de passe que je choisirais
echo '<h3>AddUser</h3>';
echo "En tant qu’utilisateur, je souhaite pouvoir me créer un compte avec mon adresse mail et un mot de passe que je choisirais";

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "http://localhost/2slamprj2eq02/src/api/users/addUser.php?&mdpU=P4ssw0rd&mailU=Test");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$response = curl_exec($ch);
curl_close($ch);

$response = json_decode($response, true);

echo '<pre>';
echo 'Response: ';
print_r($response);
echo '</pre>';

// En tant qu’utilisateur, je souhaite pouvoir me connecter avec mon adresse mail et mon mot de passe
echo '<h3>ConnectUser</h3>';
echo "En tant qu’utilisateur, je souhaite pouvoir me connecter avec mon adresse mail et mon mot de passe";

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "http://localhost/2slamprj2eq02/src/api/users/connectUser.php?&mdpU=P4ssw0rd&mailU=Test");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$response = curl_exec($ch);
curl_close($ch);

$response = json_decode($response, true);

echo '<pre>';
echo 'Response: ';
print_r($response);
echo '</pre>';

// Clean up
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "http://localhost/2slamprj2eq02/src/api/users/cleanTests.php");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_exec($ch);
curl_close($ch);